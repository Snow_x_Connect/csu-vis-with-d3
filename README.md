## 运行本项目

    npm i
    npm run start

## mysql相关配置
要么使用环境变量:

`MYSQL_HOST`:mysql所在主机地址,比如localhost

`MYSQL_USER`:连接mysql的用户名

`MYSQL_PASS`:密码

`MYSQL_DBNAME`:mysql的数据库名

`MYSQL_PORT`:mysql的端口号,比如3306

命令行:

    MYSQL_HOST=localhost MYSQL_USER=root MYSQL_PASS=xxx MYSQL_DBNAME=xxx MYSQL_PORT=xxx npm run start
***
要么在`mysql`文件夹下新建`secret.js`文件
```
module.exports = {
    hostname: "localhost",
    username: "root",
    password: "xxx",
    name: "xxx",
    port:3306
}
```

## 使用到的web api接口的数据格式
#### Scatter.vue
    {
        points:
        [
            {
                test_id:4001,
                x:-0.15532081,
                y:0.167550598144,
                label:0  //根据不同label进行染色
            },
            ...
        ]
    }

#### GlobalOverview.vue
    {
        data_range:["2016-02-16", "2016-02-17", "2016-02-18", "2016-02-19", "2016-02-20", "2016-02-21"]//字符串数组,从头到尾应该是连续的没有缺漏
        sng_bar_data:[
            {
                sng:1,
                values:[
                    {
                        $date_E8:"2016-02-16",//东八区的日期,对应横坐标
                        "COUNT(SNG_label)":2,//对应柱形图的长度,注意COUNT(SNG_label)的大小写
                    },
                    ...
                ]
            },
            {
                sng:-5,
                values:[
                    {
                        $date_E8:"2016-02-16",//东八区的日期,对应横坐标
                        "COUNT(SNG_label)":2,//对应柱形图的长度
                    },
                    ...
                ]
            },
            ... 
            //这里的所有values数组,应该是相同长度的
            //values数组之间,相同位置的一项里的$date_E8也应该是一一对应相同的
            //否则D3.js计算堆叠的时候就不对
        ],//绘制sng的堆叠柱形图
        type_bar_data:[
            {
                type:0,
                values:[
                    {
                        $date_E8:"2016-02-16",
                        "count(type)":2479 //注意count(type)的大小写
                    }
                ]
            },
            {
                type:1,
                values:[

                ]

            },
            //和sng里类似,这里的values数组也要一个个对应
        ]//绘制type的堆叠柱形图
    }

#### Trend.vue
    {
        max:1132.8,//代表纵轴的最大值
        min:-163.85//代表纵轴的最小值
        array:[
            {
                sng:-5,
                rows:[
                {
                    row_id:1,
                    v_avg:0.1946, //平均值,用来画中间的实线
                    v_max:35.3,//上界
                    v_min:-25.77//下界
                },
                {
                    row_id:2,
                    v_avg:0.01446,
                    v_max:36.93,
                    v_min:-35.77
                },
                ...]
            },
            {
                sng:0,
                rows:[...]
            },
            ...
        ]
    }