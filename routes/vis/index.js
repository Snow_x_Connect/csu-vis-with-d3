const express = require("express");
const path = require("path");
const router = express.Router();
var app = router;
var scatterPoints = require('./scatter-points');
var overview = require("./overview");
var trend = require("./trend");

app.use(express.static(path.join(__dirname, 'public/dist')));
app.use('/scatter-points', scatterPoints);
app.use("/overview", overview);
app.use('/trend', trend);




module.exports = router;