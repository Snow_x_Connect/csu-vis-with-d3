function host() {
  var hostname = location.hostname;
  var port = location.port;
  var my_host;
  if (!port) {
    my_host = hostname;
  } else {
    my_host = hostname + ':' + '3000'; //express
  }

  /**
   * 
   * @param {String} path 
   */
  const fn = function host(path) {
    return location.protocol + '//' + my_host + '/vis/' + path;
  }
  return fn;
}

module.exports = host();
