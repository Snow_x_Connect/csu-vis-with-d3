
try {
    var config = require("./secret");//if defined as const inner the try block,it can't be accessed outer the try block
} catch (e) {
    console.warn("试图读取secret.js的配置",e);
}
var mysql = require("mysql");
var pool = mysql.createPool({
    timeout:60*60*1000,
    connectTimeout:60*60*1000,
    acquireTimeout:60*60*1000,
    host: process.env["MYSQL_HOST"] || config.hostname,
    user: process.env["MYSQL_USER"] || config.username,
    password: process.env["MYSQL_PASS"] || config.password,
    database: process.env["MYSQL_DBNAME"] || config.name,
    port:process.env["MYSQL_PORT"]||config.port,
    connectionLimit: 2
});

var connections = [];

pool.on("error", function (err) {
    console.error(err);
    debugger
});
pool.query(`select * from foobar`, function (err, results, fileds) {
    if (err) {
        debugger
        console.error(err);
    } else {
        console.log(JSON.stringify(results));
    }
});


pool.getConnection(function (err, connection) {
    if (err) {
        return console.error(err);
    }
    connections.push(connection);
    connection.query("select * from foobar LIMIT 0,1", function (err, results, fileds) {
        if (err) {
            debugger
            console.error(err);
        } else {
            console.log(JSON.stringify(results));
            connection.release();
        }
    });
});

pool.on("connection", function (connection) {
    connections.push(connection);
    console.log("connection.threadId", connection.threadId);
})


module.exports = pool;