const cors = require('cors');
var router = require('express').Router();
var mysqlPool = require('./mysql');


router.get("/", cors(), function (req, res) {
    mysqlPool.query("select * from 4001_6000_mds limit 0,2000", function (err, results, fields) {
        if (err) {
            res.status(500);
            res.send(err);
        } else {
            res.send({
                points: results
            });
        }
    })
    // res.send([{ x: 0, y: 0 }]);
});

module.exports = router;